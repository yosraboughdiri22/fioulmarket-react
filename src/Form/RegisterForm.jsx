import React, { useState } from 'react';
import './RegisterForm.css';

const RegisterForm = () => {
  const [touched, setTouched] = useState({});
  const [errors, setErrors] = useState({});
  const [values, setValues] = useState({
    email: '',
    password: '',
    confirm: '',
  });

  const handleInputChange = (e, field) => {
    const { [field]: removedError, ...restErrors } = errors;

    setTouched({ ...touched, [field]: true });
    setErrors(restErrors);
    setValues({ ...values, [field]: e.currentTarget.value });
  };

  const validateForm = (formValues) => {
    let formErrors = {};

    if (!formValues.email.match(/^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$/)) {
      formErrors = {
        ...formErrors,
        email: ['You have to provide a valid email.'],
      };
    }

    if (formValues.password.length < 5 || formValues.password.length > 16) {
      formErrors = {
        ...formErrors,
        password: ['Your password must be between 5 and 16 characters length.'],
      };
    }

    if (formValues.confirm.length < 5 || formValues.confirm.length > 16) {
      formErrors = {
        ...formErrors,
        confirm: ['Your password must be between 5 and 16 characters length.'],
      };
    }

    if (formValues.confirm !== formValues.password) {
      const passwordError = errors.password
        ? [...errors.password, 'Your passwords must match.']
        : ['Your passwords must match'];

      formErrors = {
        ...formErrors,
        password: passwordError,
      };
    }

    setErrors(formErrors);

    return Object.keys(formErrors).length === 0;
  };

  const handleSubmit = (e) => {
    e.preventDefault();

    if (!validateForm(values)) {
      return;
    }

    alert(JSON.stringify(values));
  };

  const isEmailError = errors.email !== undefined && touched.email;
  const isPasswordError = errors.password !== undefined && touched.password;
  const isConfirmError = errors.confirm !== undefined && touched.confirm;

  return (
    <div className="RegisterForm">
      <h1>Registration form</h1>
      <form onSubmit={handleSubmit}>
        <div className={`form-item${isEmailError ? ' has-error' : ''}`}>
          <label htmlFor="">Email</label>
          <input type="text" value={values.email} onChange={(e) => handleInputChange(e, 'email')} />
          {isEmailError && (
            <span className="error-message">
              {errors.email.map((err, k) => (
                <span key={k} className="error-item">
                  {err}
                </span>
              ))}
            </span>
          )}
        </div>
        <div className={`form-item${isPasswordError ? ' has-error' : ''}`}>
          <label htmlFor="">Password</label>
          <input type="text" value={values.password} onChange={(e) => handleInputChange(e, 'password')} />
          {isPasswordError && (
            <span className="error-message">
              {errors.password.map((err, k) => (
                <span key={k} className="error-item">
                  {err}
                </span>
              ))}
            </span>
          )}
        </div>
        <div className={`form-item${isConfirmError ? ' has-error' : ''}`}>
          <label htmlFor="">Confirm</label>
          <input type="text" value={values.confirm} onChange={(e) => handleInputChange(e, 'confirm')} />
          {isConfirmError && (
            <span className="error-message">
              {errors.confirm.map((err, k) => (
                <span key={k} className="error-item">
                  {err}
                </span>
              ))}
            </span>
          )}
        </div>
        <div className="form-item">
          <input type="submit" value="Register !" />
        </div>
      </form>
    </div>
  );
};

export default RegisterForm;
