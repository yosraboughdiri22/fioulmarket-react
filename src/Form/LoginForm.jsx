import React, { useState } from 'react';

const LoginForm = () => {
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [errorMessage, setErrorMessage] = useState('');

  const handleSubmit = (e) => {
    e.preventDefault();

    console.log('Email:', email);
    console.log('Password:', password);

    if (!email || !password) {
      setErrorMessage('Veuillez remplir tous les champs');
      return;
    } 
    
    // Gérer la réponse du serveur (par exemple, vérifier le statut de connexion)

    
    // Réinitialiser les champs du formulaire
    setEmail('');
    setPassword('');
    setErrorMessage('');
  };

  return (
    <form onSubmit={handleSubmit}>
      <h2>Connexion</h2>
      {errorMessage && <p className="error-message">{errorMessage}</p>}
      <div>
        <label htmlFor="email">Email:</label>
        <input
          type="email"
          id="email"
          value={email}
          onChange={(e) => setEmail(e.target.value)}
          required
        />
      </div>
      <div>
        <label htmlFor="password">Mot de passe:</label>
        <input
          type="password"
          id="password"
          value={password}
          onChange={(e) => setPassword(e.target.value)}
          required
        />
      </div>
      <button type="submit">Se connecter</button>
    </form>
  );
};

export default LoginForm;
