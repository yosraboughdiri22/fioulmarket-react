import React from 'react';
import TabContainer from './Tab/TabContainer';
import './App.css';

function App() {
  return (
    <div className="App">
      <TabContainer />
    </div>
  );
}

export default App;
