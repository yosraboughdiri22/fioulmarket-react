import React, { useState } from 'react';
import RegisterForm from '../Form/RegisterForm';
import LoginForm from '../Form/LoginForm';

const LOGIN = 'login';
const REGISTER = 'register';

const TabContainer = () => {
  const [activeTab, setActiveTab] = useState(LOGIN);

  const handleTabChange = (tab) => {
    setActiveTab(tab);
  };

  return (
    <div className="tab-container">
      <TabHeader filter={LOGIN} activeTab={activeTab} onTabChange={handleTabChange}>
        J'ai un compte
      </TabHeader>
      <TabHeader filter={REGISTER} activeTab={activeTab} onTabChange={handleTabChange}>
        Je n'ai pas de compte
      </TabHeader>

      <TabContent filter={LOGIN} activeTab={activeTab}>
        <LoginForm />
      </TabContent>
      <TabContent filter={REGISTER} activeTab={activeTab}>
        <RegisterForm />
      </TabContent>
    </div>
  );
};

const TabHeader = ({ filter, activeTab, onTabChange, children }) => {
  const isActive = activeTab === filter;

  const handleClick = () => {
    onTabChange(filter);
  };

  return (
    <div className="tab-header">
      <button onClick={handleClick} className={isActive ? 'active' : ''}>
        {children}
      </button>
    </div>
  );
};

const TabContent = ({ filter, activeTab, children }) => {
  if (filter !== activeTab) {
    return null;
  }

  return <div className="content">{children}</div>;
};



export default TabContainer;
